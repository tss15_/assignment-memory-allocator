#include "test.h"

int main() {
    enum status status = test_one();
    write_status(status,1);

    status = test_two();
    write_status(status,2);

    status = test_three();
    write_status(status,3);

    status = test_four();
    write_status(status,4);

    status = test_five();
    write_status(status,5);

    return 0;
}

