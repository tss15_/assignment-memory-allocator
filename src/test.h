#ifndef ALLOCATOR_TEST_H
#define ALLOCATOR_TEST_H

enum status test_one();
enum status  test_two();
enum status test_three();
enum status test_four();
enum status test_five();
void write_status(const enum status status, int num);
enum status  {
    SUCCESS ,
    BLOCK_IS_NULL,
    BLOCK_LOCATING_ERR,
    BLOCK_IS_FREE_ERR,

};

#endif //ALLOCATOR_TEST_H

